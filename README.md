# oo-beets


## Library structure

### Folders

`Alexander` - My library, managed with beets
`Shared` - Shared library, managed with beets
`Elisabet` - Elisabet's library, not managed by beets

Moving albums between folders will break Playlists in Nextcloud Music, which can be solved by exporting > editing > importing any affected playlists.

### converted_from_lossy

Albums/singletons that are in the flac format but have been converted from lossy formats for compatibility reasons have a custom field `converted_from_lossy` set to `true`.


## General Usage

Enter `beetsdir` and then the directory that corresponds to the environment you are planning to interact with.

Then run the automated import tooling or manually run beets CLI commands as described below. 



## Automated import tooling

**Attention: For production use, run these commands from the beets container on the server.**

Run the make targets from the environment's Makefile.

The most important targets of the `prod` environment:


### `rip-cd`

Example: `make rip-cd`

Rips the Audio-CD in the disc drive to flac files into the `ALBUM_IMPORT_PATH`.

This is intended to be run on a client computer, not the server.

Initial metadata is interactively queried from CDDB.


### `import`

Example: `make import source=B*`

Import from the `ALBUM_IMPORT_PATH` into the `ALBUM_LIBRARY_PATH` specified in the environment's `.envrc` file.

You need to specify a folder name or globbing pattern using the `source` parameter.

A backup copy of the sqlite library file is created before every import.


### `finish-import`

Example: `make finish-import`

Wraps up the currently open import.
This clears the `import` tag from all files, runs `eefilter-clear`, and clears the current import logs.

Do this after having resolved all verification warnings of a prior import have been resolved to prepare for a new import.


### `verify`

Example: `verify`

Shows known problems and stats about library. Run this to verify that you have successfully resolved any detected problems after an import.


### Cover art

List albums with missing value on artpath field:

```
beet ls -a 'artpath::^$'
```

Set cover art from local file or download if necessary (only works on albums with missing cover art):

```
beet -v fetchart album:"[...]"
```

Update albumart from remote sources (this will ignore any existing local files!):

```
beet -v fetchart -f album:"[...]"
```

If there is a file in the album folder that has a name matching any of `cover front art album folder`, it will be used for the `cover.jpg`.

If there already is a file `cover.jpg`, the artpath file will be called `cover.1.jpg`. To avoid that, rename the file to `folder.jpg`. This will fix the `artpath` field and skip image processing.


### Embedded cover art

To embed the album cover into each file:

```
beet embedart QUERY
```

Remove emdedded art from queried files:

```
beet clearart QUERY
```

Remove emdedded art from all singletons:

```
beet clearart singleton:true
```



## beets CLI

beets CLI calls need to be prefixed with `pipenv run`.


### Import

Import dry-run:

```
beet import -C -W {{ path }}
```

Import while setting fields, e.g.:

```
beet import --set genre="Folk" --set mood="emotional"
```


### Fields

Print a list of fields that are available in library:

```
beet fields
```


### list

To query the database: 

```
beet ls albumartist:name
```

To get file paths instead of the format string, use the `-p` flag:

```
beet ls -p albumartist:name
```

Search is fuzzy by default. For exact matches:

```
beet ls albumartist:=~"abc" # case-insensitive
beet ls albumartist:="ABC" # case-sensitive
```

To use use regex:

```
beet ls albumartist::"^beck$"
```

Use a `,` as OR operator:

```
beet ls album:"abc" , album:"123"
```

### list all tags on a track

Use the [info plugin](https://beets.readthedocs.io/en/stable/plugins/info.html) to list all tags that beets itself parses/supports:

```
beet info album:name track:1
```

Use mutagen-inspect to show all tags present in a file:

```
mutagen-inspect /home/ubuntu/.mount/audio/Musiclibrary/Alexander/Albums/[...]flac
```


### Stats

```
beet stats
```

Show basic stats about the library


### Update library

#### MusicBrainz

Sync library with fresh data from MusicBrainz:

```
beet mbsync {{ query }}
```

#### From files

To update the database from files, run the below command.

```
beet update -F albumartist
```

A normal `beet update` works as well, but slower and is potentially destructive to certain data, such as acousticbrainz data. So don't run this on the whole collection. `beets` will report that it will truncate genres, the `multi-genre` plugin does however prevent that before the files are written


If you need to force an update, because otherwise files are ignored because of modification times not changed:

```
find ~/.mount/audio/Musiclibrary/Alexander/Albums/ -exec touch {} \;
```

Dry run:

```
beet update -p
```

If the above does not work, one can do a re-import of the tracks instead:

```
beet import -L album:"name"
```

#### Compilations

Mark album as compilation

```
beet modify comp=1 albumtype=compilation -a QUERY
beet modify comp=1 albumtype=compilation QUERY
```

Revert compilation to nortmal album

```
beet modify comp=0 albumtype! -a QUERY
beet modify comp=0 albumtype! QUERY
```

#### Discs

Set disc to 1/1:

```
beet modify album:"name" disc=1 disctotal=1
```

#### Genre

With the [lastgenre](https://docs.beets.io/en/stable/plugins/lastgenre.html) plugin, I use canonicalization with the default [genre tree](https://raw.githubusercontent.com/beetbox/beets/master/beetsplug/lastgenre/genres-tree.yaml) and a custom white list (allowed-genres.list).

Set genre tags on albums/tracks with lastgenre:

```
beet lastgenre {{ query }}
beet lastgenre -A singleton:true {{ query }}
```

Set genre tags with lastgenre on all albums/tracks with incomplete tags:

```
beet lastgenre 'genre::^((?!,).)*$'
beet lastgenre -A singleton:true 'genre::^((?!,).)*$'
```

Edit genre tag for album:

```
beet modify -a album:"name" genre="tag, tag"
```

Edit genre tag for track in album:

```
beet modify album:"name" genre="tag, tag"
```

To filter for multi-value genre fields:

```
eefilter -a albumartist::"^A" 'genre::^.+,.+$'
```

#### Original year

```
beet modify album:"name" original_year=2010
```

#### Playlists

Update smart playlists:

```
beet splupdate
```

#### Move albums

You can move albums to different folders using the `move` command.

If no query is supplied, all files will be moved to configured library folder.
If the `-d` parameter is omitted, the library directory is used.

Use `-p` for dry run mode.
The `-c` option copies files instead of moving them.
The `-e` flag (for “export”) copies files without changing the database.

```
beet move -d DIR -a album:"name"
```

Source: https://beets.readthedocs.io/en/stable/reference/cli.html#move

### Remove albums

To remove an album from the library AND delete the corresponding files on disk:

```
beet rm -d -a album:"name"
```

#### Move album to Elisabet's collection and remove from database

```
beet move -d ~/.mount/audio/Musiclibrary/Elisabet/Albums/ -a album:"name"
beet rm -a album:"name"
```

#### Move album to Shared collection

```
beet move -d ~/.mount/audio/Musiclibrary/Shared/Albums/ -a album:"name"
```

#### Rescan library in Nextcloud Music

Scan for new/removes/changed files:

```
run-occ files:scan --path=/alexander/files/Audio/Musiclibrary;\
run-occ music:scan --clean-obsolete alexander elisabet;\
run-occ music:reset-cache --all
```

Force rescan of already scanned files:

```
run-occ music:scan --rescan --folder="/Audio/Musiclibrary/Alexander/Albums/Hiroshi Yoshimura/"
```

### Import Playlists

To import all playlists from a folder, run this:

```
run-occ music:playlist-import --file "/Audio/Musiclibrary/Alexander/Playlists/*.m3u" alexander
```


## How to handle singles/singetons

Singles that are separate releases, should be treated just like albums.

Another case is when you have separate files taken from albums lying about. These should be treated as singletons. beets has a built-in mechanism to handle singletons.

Singletons can be imported using:

```
beet import -s {{ path }}
```

Then make sure to remove any album information from these tracks to avoid them showing up as incomplete albums in some players.

```
beet modify singleton:true album= albumartist=
```

Or you can merge the two commands above like this:

```
beet import -s --set album= --set albumartist=
```

Sources:
 * https://www.blisshq.com/music-library-management-blog/2010/11/09/how-to-organise-mp3-singles/
 * https://discourse.beets.io/t/singletons-and-kodi/246
 * https://kodi.tv/article/singles-support-music-library



## Possibly useful plugins to check out

* Integrity check: https://github.com/geigerzaehler/beets-check
* Smart playlists: https://beets.readthedocs.io/en/stable/plugins/smartplaylist.html
* Acoustid fingerprinting for completely unknown material: https://beets.readthedocs.io/en/stable/plugins/chroma.html



## Install ReplayGain plugin with GStreamer

Register system Python 3 in pyenv:

```
git clone https://github.com/doloopwhile/pyenv-register.git $(pyenv root)/plugins/pyenv-register
exec "$SHELL"
pyenv register /usr/bin/python3.6
```

Install system dependencies and create pipenv with system Python 3 and site-packages enabled:

```
apt install python3-gi
pipenv --rm || true
pipenv --three --site-packages --python 3.6.8
PIP_IGNORE_INSTALLED=1 pipenv install
```



## Install plugins from github in pipenv

For example:

```
pipenv install git+git://github.com/Holzhaus/beets-copyartifacts.git#egg=beets-copyartifacts-holzhaus
```
