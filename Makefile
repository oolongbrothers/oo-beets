# pipenv

.PHONY: pipenv-create
pipenv-create:
	pipenv --site-packages
	pipenv sync
	pipenv run pip uninstall -y mutagen

.PHONY: pipenv-create-dev
pipenv-create-dev:
	pipenv --site-packages
	pipenv sync --dev
	pipenv run pip uninstall -y mutagen

# composite targets

.PHONY: install
install: pipenv-create

.PHONY: install-dev
install-dev: pipenv-create-dev

.PHONY: uninstall
uninstall: pipenv-clean

# CD ripper installation for local computer

.PHONY: install-abcde-dependencies
install-abcde-dependencies:
	sudo apt install -y abcde libmusicbrainz-discid-perl libwebservice-musicbrainz-perl flac --no-install-recommends
	ln -sf ${PWD}/abcde/abcde.conf ${HOME}/.abcde.conf

# beets depends on mutagen, we want to use the apt-installed mutagen though
# because it is compiled with support for replay gain analysis as opposed to
# the pypi package.
# A pipenv sync will install the pypi mutagen package, that is why we need
# to uninstall the it afterwards (see pipenv-create targets) to stop it
# shadowing the apt package.

.PHONY: install-system-mutagen
install-system-mutagen:
	sudo apt install -y python3-mutagen
