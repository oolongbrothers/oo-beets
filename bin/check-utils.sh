#!/usr/bin/env bash

BOLD='\033[1m'
RED='\033[0;31m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
NC='\033[0m' # No Color

function print_title() {
    echo -e "${BOLD}${1}:${NC}"
}

function print_ok() {
    echo -e "${GREEN}ok${NC}"
}

function print_fail() {
    echo -en "${RED}fail"
    if [ -n "$1" ]; then
        echo -en " $1"
    fi
    echo -en "${NC}"
    echo ""
}

function print_output() {
    if [ -n "$output" ]; then
        echo -e "${CYAN}$1${NC}"
    fi
}

function print_tip() {
    if [ -n "${1}" ]; then
        echo -e "Try this to fix:\n${1}"
    fi
}

function run_command_check() {
    print_title "$1"
    echo -ne "${CYAN}"
    output=$(eval "${2}")
    echo -ne "${NC}"
    if [ $? -eq 0 ]; then
        print_ok
    else
        print_output "$output"
        print_fail
        print_tip "$3"
    fi
    echo ""
}

function run_output_check() {
    print_title "$1"
    echo -ne "${CYAN}"
    output=$(eval "${2}")
    echo -ne "${NC}"
    if [ -z "$output" ]; then
        print_ok
    else
        print_output "$output"
        print_fail
        print_tip "$3"
    fi
    echo ""
}

run_file_absent_check() {
    print_title "$1"
    if [ -e "${2}" ]; then
        print_fail "(file/directory found)"
        print_tip "$3"
    else
        print_ok
    fi
    echo ""
}
