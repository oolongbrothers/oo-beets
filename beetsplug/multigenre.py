from beets.plugins import BeetsPlugin
from beets.library import Album, Item
from mediafile import MediaFile
from beets import ui
from copy import copy
from beets import logging

GENRE_SEPARATOR = ', '


def join_genres(genres: list) -> str:
    if genres:
        return GENRE_SEPARATOR.join(genres)


def split_genres(genre: str) -> list:
    if genre:
        return [g.strip() for g in genre.split(GENRE_SEPARATOR)]
    else:
        return None


def load_from_file(item) -> str:
    file = MediaFile(item.path)
    return file.genres


def have_genres_changed(genres_a: str, genres_b: str) -> bool:
    return genres_a != genres_b


def genres_seem_truncated(split_genres_list: list) -> bool:
    return split_genres_list and len(split_genres_list) == 1


def restore_genres_from_file(item):
    input_genres = item.genre
    file_genres = load_from_file(item)
    joined_file_genres = join_genres(file_genres)
    if file_genres and have_genres_changed(input_genres, joined_file_genres):
        item.genre = joined_file_genres
        item.store()
        return file_genres
    else:
        return None


class MultiGenrePlugin(BeetsPlugin):

    def __init__(self):
        super(MultiGenrePlugin, self).__init__()
        self.register_listener('import_task_created', self.import_task_created)
        self.register_listener('write', self.write)
        # Use before_item_moved event to fix update truncating genre
        #   since database_change does not allow for triggering further db changes
        self.register_listener('before_item_moved', self.before_item_moved)

    def import_task_created(self, task, session):
        for item in task.items:
            input_genres = item.genre
            file_genres = load_from_file(item)
            joined_file_genres = join_genres(file_genres)
            if file_genres and have_genres_changed(item.genre, joined_file_genres):
                item.genre = joined_file_genres
                self._log_correction(item, input_genres, joined_file_genres)

    def write(self, item, path, tags):
        input_genres = tags["genre"] if "genre" in tags else None
        if input_genres:
            split_input_genres = split_genres(input_genres)
            target_genres = split_input_genres
            tags["genres"] = target_genres
            if self._log.isEnabledFor(logging.DEBUG):
                self._log.debug(
                    f"Converted genre to multi-field on save of album {format(item)}: '{target_genres}'")
            if genres_seem_truncated(target_genres):
                if self._log.isEnabledFor(logging.DEBUG):
                    self._log.debug(ui.colorize(
                        "text_warning",
                        f"Possibly incomplete genres for {format(item)}: '{target_genres}'"))

    def before_item_moved(self, item, source, destination):
        input_genres = item.genre
        if input_genres and genres_seem_truncated(split_genres(input_genres)):
            if isinstance(item, Item):
                file_genres = restore_genres_from_file(item)
                if genres_seem_truncated(file_genres):
                    if self._log.isEnabledFor(logging.DEBUG):
                        self._log.debug(ui.colorize(
                            "text_warning",
                            f"Possibly incomplete genres for {format(item)}: '{file_genres}'"))
                else:
                    self._log_correction(
                        item, input_genres, join_genres(file_genres))
            else:
                self._log.warn(
                    ui.colorize("text_warning",
                                f"Unknown item type: {str(item._type)}"))

    def _log_correction(self, item, old_genres: str, new_genres: str) -> None:
        if self._log.isEnabledFor(logging.DEBUG):
            self._log.debug(
                f"Corrected genres for {format(item)}: {old_genres} -> {new_genres}")

    def _show_diff(self, model):
        model_changed = ui.show_model_changes(model)
        if isinstance(model, Album):
            for item in model.items():
                temp_item = copy(item)
                temp_item['genre'] = model['genre']
                model_changed = ui.show_model_changes(temp_item) \
                    or model_changed
        return model_changed

    def _align_album_item_genre(self, album):
        album_value = album['genre']
        for item in album.items():
            self._store_if_necessary(album_value, item)

    def _store_if_necessary(self, genre, model):
        is_different = _is_genre_different(genre, model)
        if is_different:
            model['genre'] = genre
            model.store()
        else:
            if self._log.isEnabledFor(logging.DEBUG):
                self._log.debug(
                    f'genre "{genre}" was already up to date for {model.albumartist}.')
        return is_different


def _is_genre_different(genre, model):
    return 'genre' not in model or model['genre'] != genre
