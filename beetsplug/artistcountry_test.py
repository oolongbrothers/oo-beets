import unittest
from test_helper import MockItem, MockTask
from artistcountry import ArtistCountryPlugin, prettify_country_name
from musicbrainzngs.musicbrainz import set_useragent


class TestArtistCountry(unittest.TestCase):

    def setUp(self):
        set_useragent("beets-artistcountry-field-oolongbrothers-integration-tests", "0.0.1",
                      "https://gitlab.com/oolongbrothers/oo-beets")

    # = prettify ==============================================================

    def test_prettify__known_artist(self):
        plugin = ArtistCountryPlugin()
        country = plugin.prettify("DE")
        self.assertEqual("Germany", country)

    # = prettify_country_name ================================================

    def test_prettify_country_name__unknown(self):
        with self.assertRaises(ValueError):
            prettify_country_name("sfd")

    def test_prettify_country_name__worldwide(self):
        pretty_country = prettify_country_name("XW")
        self.assertEqual("Worldwide", pretty_country)

    def test_prettify_country_name__germany(self):
        pretty_country = prettify_country_name("DE")
        self.assertEqual("Germany", pretty_country)

    def test_prettify_country_name__congo(self):
        pretty_country = prettify_country_name("CD", ", ")
        self.assertEqual(
            "Congo_ The Democratic Republic of the", pretty_country)


if __name__ == '__main__':
    unittest.main()
