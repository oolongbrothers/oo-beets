class MockTask(object):
    def __init__(self, **kwargs):
        self.items = [
            MockItem(**kwargs),
        ]
        self.is_album = False
        self.item = MockItem(**kwargs),


class MockItem(dict):
    def __init__(self, **kwargs):
        if kwargs:
            for key, value in kwargs.items():
                self[key] = value

    def __getattr__(self, key):
        return self[key]

    def __setattr__(self, key, value):
        self[key] = value

    def store(self):
        pass
