import unittest
from test_helper import MockItem, MockTask
from multigenre import MultiGenrePlugin, join_genres, split_genres, genres_seem_truncated


class TestJoinGenres(unittest.TestCase):

    # = join_genres ===========================================================

    def test_join_genres__None(self):
        genre_list = None
        self.assertEqual(None, join_genres(genre_list))

    def test_join_genres__empty(self):
        genre_list = []
        self.assertEqual(None, join_genres(genre_list))

    def test_join_genres__single(self):
        genre_list = ["genre1"]
        self.assertEqual("genre1", join_genres(genre_list))

    def test_join_genres__list(self):
        genre_list = ["genre1", "genre2", "genre3"]
        self.assertEqual("genre1, genre2, genre3", join_genres(genre_list))


class TestSplitGenres(unittest.TestCase):

    # = join_genres ===========================================================

    def test_split_genres__none(self):
        genre = None
        self.assertEqual(None, split_genres(genre))

    def test_split_genres__empty(self):
        genre = ''
        self.assertEqual(None, split_genres(genre))

    def test_split_genres__single(self):
        genre = "genre1"
        self.assertEqual(["genre1"], split_genres(genre))

    def test_split_genres__list(self):
        genre = "genre1, genre2, genre3"
        self.assertEqual(["genre1", "genre2", "genre3"], split_genres(genre))


class TestGenresSeemTruncated(unittest.TestCase):

    def test_genres_seem_truncated__none(self):
        self.assertFalse(genres_seem_truncated(None))

    def test_genres_seem_truncated__empty(self):
        self.assertFalse(genres_seem_truncated([]))

    def test_genres_seem_truncated__long_list(self):
        genre_list = ["genre1", "genre2", "genre3"]
        self.assertFalse(genres_seem_truncated(genre_list))

    def test_genres_seem_truncated__truncated_list(self):
        genre_list = ["genre1"]
        self.assertTrue(genres_seem_truncated(genre_list))


class TestMultiGenrePlugin(unittest.TestCase):

    # = write =================================================================

    def test_write__joined_genre(self):
        plugin = MultiGenrePlugin()
        path = "./beetsplug/test_resources/Entertainment for the Braindead - Paper - joined.ogg"
        item = MockItem(
            album="album1",
            path=path,
        )
        tags = MockItem(genre="Folk, Indie, Songwriter, Cologne, Germany")
        plugin.write(item, path, tags)
        self.assertEqual(
            ["Folk", "Indie", "Songwriter", "Cologne", "Germany"], tags.genres)

    # = import_task_created =================================================================

    def test_import_task_created__joined_genre(self):
        plugin = MultiGenrePlugin()
        task = MockTask(
            album="album1",
            genre="Folk",
            path="./beetsplug/test_resources/Entertainment for the Braindead - Paper - joined.ogg",
        )
        plugin.import_task_created(task, None)
        self.assertEqual(
            "Folk, Indie, Songwriter, Cologne, Germany", task.items[0].genre)

    def test_import_task_created__joined_genre_already_correct(self):
        plugin = MultiGenrePlugin()
        task = MockTask(
            genre="Folk, Indie, Songwriter, Cologne, Germany",
            path="./beetsplug/test_resources/Entertainment for the Braindead - Paper - joined.ogg",
        )
        plugin.import_task_created(task, None)
        self.assertEqual(
            "Folk, Indie, Songwriter, Cologne, Germany", task.items[0].genre)

    def test_import_task_created__single_genre(self):
        plugin = MultiGenrePlugin()
        task = MockTask(
            genre="Folk",
            path="./beetsplug/test_resources/Entertainment for the Braindead - Paper - single.ogg",
        )
        plugin.import_task_created(task, None)
        self.assertEqual(
            "Folk", task.items[0].genre)

    def test_import_task_created__already_split_genre(self):
        plugin = MultiGenrePlugin()
        task = MockTask(
            album="album1",
            genre="Folk",
            path="./beetsplug/test_resources/Entertainment for the Braindead - Paper - split.ogg",
        )
        plugin.import_task_created(task, None)
        self.assertEqual(
            "Folk, Indie, Songwriter, Cologne, Germany", task.items[0].genre)


if __name__ == '__main__':
    unittest.main()
