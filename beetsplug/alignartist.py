from beets.plugins import BeetsPlugin
from beets.ui import Subcommand
from beets.library import Album
from beets import ui
from copy import copy
from beets import logging


class AlignArtistPlugin(BeetsPlugin):

    def __init__(self):
        super(AlignArtistPlugin, self).__init__()

    def commands(self):
        # = Subcommand: aligngenre ======================

        aligngenre_subcommand = Subcommand(
            'aligngenre', aliases=("alge",), help=f'Aligns genres for artist/albumartist')
        aligngenre_subcommand.parser.add_album_option()
        aligngenre_subcommand.parser.add_option(
            '-p', '--pretend',
            action="store_true",
            help='dry run without applying changes',
        )
        aligngenre_subcommand.parser.add_option(
            '-r', '--artist',
            action="store",
            type="string",
            help=f'The artist to use for finding genres',
        )

        def _aligngenre_subcommand_function(lib, opts, args):
            return self._align(lib, opts, args, 'genre')
        aligngenre_subcommand.func = _aligngenre_subcommand_function

        # = Subcommand: aligncountry ======================

        aligncountry_subcommand = Subcommand(
            'aligncountry', aliases=("alco",), help=f'Aligns genres for artist/albumartist')
        aligncountry_subcommand.parser.add_album_option()
        aligncountry_subcommand.parser.add_option(
            '-p', '--pretend',
            action="store_true",
            help='dry run without applying changes',
        )
        aligncountry_subcommand.parser.add_option(
            '-r', '--artist',
            action="store",
            type="string",
            help=f'The artist to use for finding genres',
        )

        def _aligncountry_subcommand_function(lib, opts, args):
            return self._align(lib, opts, args, 'albumartist_country')
        aligncountry_subcommand.func = _aligncountry_subcommand_function

        # = Subcommand: alignartistid ======================

        alignartistid_subcommand = Subcommand(
            'alignartistid', aliases=("alai",), help=f'Aligns artist id for artist/albumartist')
        alignartistid_subcommand.parser.add_album_option()
        alignartistid_subcommand.parser.add_option(
            '-p', '--pretend',
            action="store_true",
            help='dry run without applying changes',
        )
        alignartistid_subcommand.parser.add_option(
            '-r', '--artist',
            action="store",
            type="string",
            help=f'The artist to use for finding genres',
        )

        def _alignartistid_subcommand_function(lib, opts, args):
            return self._align(lib, opts, args, 'mb_artistid')
        alignartistid_subcommand.func = _alignartistid_subcommand_function

        # = Subcommand: alignalbumartistid ======================

        alignalbumartistid_subcommand = Subcommand(
            'alignalbumartistid', aliases=("alaai",), help=f'Aligns artist id for artist/albumartist')
        alignalbumartistid_subcommand.parser.add_album_option()
        alignalbumartistid_subcommand.parser.add_option(
            '-p', '--pretend',
            action="store_true",
            help='dry run without applying changes',
        )
        alignalbumartistid_subcommand.parser.add_option(
            '-r', '--artist',
            action="store",
            type="string",
            help=f'The artist to use for finding genres',
        )

        def _alignalbumartistid_subcommand_function(lib, opts, args):
            return self._align(lib, opts, args, 'mb_albumartistid')
        alignalbumartistid_subcommand.func = _alignartistid_subcommand_function

        return [aligngenre_subcommand,
                aligncountry_subcommand,
                alignartistid_subcommand,
                alignalbumartistid_subcommand]

    def _align(self, lib, opts, args, field):
        arguments = ui.decargs(args)
        selection = None
        if arguments:
            if opts.album:
                selection = lib.albums(arguments)
            else:
                selection = lib.items(arguments)
        else:
            ui.print_(
                ui.colorize("text_warning", f'You need to supply a query'))
            return
        if len(selection) == 0:
            return

        # output matched items
        ui.print_(
            f'Editing the following {"albums" if opts.album else "songs"}:')
        if opts.album:
            for album in selection:
                ui.print_(format(album))
        else:
            for item in selection:
                ui.print_(format(item))

        # collect field value options from queried music
        field_values, field_unset_detected = _collect_field_values(field,
                                                                   selection)

        # collect field value options from same or specified artist
        for artist_field in ('artist', 'albumartist'):
            query = None
            if opts.artist:
                query = f'{artist_field}:"{opts.artist}"'
            elif f'{artist_field}:' in arguments:
                query = _isolate_field_query(arguments, artist_field)
            field_values.update(
                _fetch_field_values_for_query(query, lib, field))

        # corner case handling
        if len(field_values) == 1 and not field_unset_detected:
            ui.print_(
                ui.colorize("text_warning",
                            f'{field} for artist already aligned: '
                            f'{list(field_values)[0]}'))
            return
        elif len(field_values) == 0:
            ui.print_(
                ui.colorize("text_warning",
                            f'No {field} values found.'))
            return

        # output field value options
        field_values_list = list(field_values)
        field_values_options = [
            (field_values_list.index(x), x) for x in field_values_list]
        field_values_output = '\t\n'.join(
            [f'{i + 1}. {value}' for i, value in field_values_options])
        ui.print_(
            f'\nAvailable {field} values to apply to all listed songs/albums:\n{field_values_output}')
        resp = ui.input_options(
            (u'aBort',),
            require=False,
            prompt=f'# selection (default '
            + ui.colorize('action_default', str(1))
            + '), a' + ui.colorize('action', 'B') + 'ort?',
            numrange=(1, len(field_values_options)),
            default=1)

        # evaluate input result
        if resp == 'b':
            return
        models_to_change = _assess_models_to_change(
            field_values_list, resp, selection, field)

        # apply changes
        if models_to_change and not opts.pretend and ui.input_yn('Update? (Y/n)?', require=False):
            for model in models_to_change:
                if self._log.isEnabledFor(logging.DEBUG):
                    self._log.debug(
                        f'Updating {field} for: {format(model)}')
                model.store()
                if isinstance(model, Album):
                    if self._log.isEnabledFor(logging.DEBUG):
                        self._log.debug(
                            f'Updating {field} for all tracks in album: {format(model)}')
                    self._align_album_item_field(field, model)

    def _align_album_item_field(self, field, album):
        album_value = album[field]
        for item in album.items():
            self._store_if_necessary(field, album_value, item)

    def _store_if_necessary(self, field, value, model):
        is_different = _is_field_different(field, value, model)
        if is_different:
            model[field] = value
            model.store()
        else:
            if self._log.isEnabledFor(logging.DEBUG):
                self._log.debug(
                    f'{field} "{value}" was already up to date for {model.albumartist}.')
        return is_different


def _is_field_different(field, value, model):
    return field not in model or model[field] != value


def _collect_field_values(field, selection):
    field_values = set()
    field_unset_detected = False
    for model in selection:
        if field in model and model[field] != "":
            field_values.add(model[field])
        elif not field_unset_detected:
            field_unset_detected = True
    return field_values, field_unset_detected


def _isolate_field_query(arguments, query_field):
    for argument in arguments.split(' '):
        if argument.startswith(query_field):
            return argument


def _fetch_field_values_for_query(artist_query, lib, field):
    artist_album_selection = None
    artist_item_selection = None
    artist_field_values = set()
    if artist_query:
        artist_album_selection = lib.albums(artist_query)
        if artist_album_selection:
            artist_album_field_values, _ = _collect_field_values(field,
                                                                 artist_album_selection)
            artist_field_values.update(artist_album_field_values)
        artist_item_selection = lib.items(artist_query)
        if artist_item_selection:
            artist_item_field_values, _ = _collect_field_values(field,
                                                                artist_item_selection)
            artist_field_values.update(artist_item_field_values)
    return artist_field_values


def _show_diff(field, model):
    model_changed = ui.show_model_changes(model)
    if isinstance(model, Album):
        for item in model.items():
            temp_item = copy(item)
            temp_item[field] = model[field]
            model_changed = ui.show_model_changes(temp_item) \
                or model_changed
    return model_changed


def _assess_models_to_change(field_values_list, resp, selection, field):
    target_field = field_values_list[int(resp) - 1]
    models_to_change = set()
    changed = False
    for model in selection:
        model[field] = target_field
        this_changed = _show_diff(field, model)
        if this_changed and not changed:
            changed = this_changed
        if this_changed:
            models_to_change.add(model)
    return models_to_change
