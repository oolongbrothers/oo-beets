from beets import logging
from beets.plugins import BeetsPlugin
from beets import ui
from beets.library import Item
from mediafile import MediaFile
from multigenre import split_genres, join_genres, GENRE_SEPARATOR
from artistcountry import prettify_country_name
from typing import List


CONVERSION_TABLE = {
    "U.K.": "United Kingdom",
    "U.S.A.": "United States",
}
COUNTRY_KEY = 'albumartist_country'


class AlbumartistCountryGenrePlugin(BeetsPlugin):

    def __init__(self):
        super(AlbumartistCountryGenrePlugin, self).__init__()
        self.config.add({
            'auto': True,
        })
        if self.config['auto'].get(bool) == True:
            self.import_stages = [self.imported]

    def commands(self):
        countrygenre_subcommand = ui.Subcommand(
            'countrygenre', aliases=("cg",), help=f'Append {COUNTRY_KEY} to genre, if neccessary')
        countrygenre_subcommand.parser.add_album_option()

        def _countrygenre_subcommand_function(lib, opts, args):
            if opts.album:
                for album in lib.albums():
                    for item in album.items():
                        if self.append(item):
                            item.write()
                    self.append(album)
            else:
                for item in lib.items():
                    if self.append(item):
                        item.write()

        countrygenre_subcommand.func = _countrygenre_subcommand_function
        return [countrygenre_subcommand]

    def imported(self, session, task):
        if task.is_album:
            self.append(task.album)
            for item in task.items:
                self.append(item)
        else:
            self.append(task.item)

    def append(self, item):
        if COUNTRY_KEY in item:
            result = _append_country_to_genre_if_required(item)
            if result:
                if self._log.isEnabledFor(logging.INFO):
                    self._log.info(
                        f"{format(item)}:\n"
                        f"{COUNTRY_KEY} ({item[COUNTRY_KEY]}) "
                        f"was appended to genre: {item['genre']}")
                return True
            else:
                if self._log.isEnabledFor(logging.DEBUG):
                    self._log.debug(
                        f"{format(item)}:\n"
                        f"{COUNTRY_KEY} ({item[COUNTRY_KEY]}) "
                        f"was already appended to genre: {item['genre']}")
                return False
        else:
            artist = None
            if (('albumartist' in item and item['albumartist'] != '')
                    or ('artist' in item and item['artist'] != '')):
                artist = item['albumartist'] if (
                    'albumartist' in item and item['albumartist'] != '') else item['artist']
                if artist != "Various Artists":
                    self._log.warn(
                        ui.colorize("text_warning",
                                    f"Item: {format(item)}:\n"
                                    f"No {COUNTRY_KEY} found for artist or albumartist: {artist}"))
            else:
                self._log.warn(
                    ui.colorize("text_warning",
                                f"Item: {format(item)}:\n"
                                f"No {COUNTRY_KEY} found, since there is no artist or albumartist. "))
            return False


def _append_country_to_genre_if_required(item):
    country_code = item[COUNTRY_KEY]
    country_name = prettify_country_name(country_code,
                                         replace_separator=GENRE_SEPARATOR)
    if country_name:
        genre = item["genre"] if "genre" in item else None
        last_genre_entry = None
        if genre:
            new_genres = split_genres(genre)
            last_genre_entry = new_genres[-1]
        else:
            new_genres = []
        if last_genre_entry and last_genre_entry in CONVERSION_TABLE.keys() \
                and CONVERSION_TABLE[last_genre_entry] == country_name:
            new_genres[-1] = country_name
            item.genre = join_genres(new_genres)
            item.store()
            if isinstance(item, Item):
                _save_genre_to_file(item, new_genres)
            return True
        elif not last_genre_entry == country_name:
            new_genres.append(country_name)
            item.genre = join_genres(new_genres)
            item.store()
            return True
        else:
            return False


def _save_genre_to_file(item: Item, new_genres: List[str]) -> None:
    file = MediaFile(item.path)
    update_dict = {
        "genres": new_genres
    }
    file.update(update_dict)
    file.save()
