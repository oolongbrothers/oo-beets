from beets.plugins import BeetsPlugin
from beets import ui
from beets.ui import Subcommand
from beets.library import Album
from beets import logging
from musicbrainzngs.musicbrainz import get_artist_by_id, get_area_by_id, ResponseError
from copy import copy
import pycountry

UNKNOWN_STRING = 'Unknown'
UNKNOWN_CODE = '?'
UNKNOWN_ARTIST_ID = 'unknown_artist_id'
WORLDWIDE_STRING = 'Worldwide'
WORLDWIDE_CODE = 'XW'
COUNTRY_KEY = 'albumartist_country'
REPLACED_SEPARATOR_PLACEHOLDER = '_ '


def prettify_country_name(country_code: str, replace_separator: str = None) -> str:
    if not country_code or country_code == UNKNOWN_CODE:
        return UNKNOWN_STRING
    if country_code == WORLDWIDE_CODE:
        return WORLDWIDE_STRING
    resolved_country = pycountry.countries.get(alpha_2=country_code)
    if resolved_country:
        country_name = resolved_country.name
        if replace_separator:
            country_name = country_name.replace(replace_separator,
                                                REPLACED_SEPARATOR_PLACEHOLDER)
        return country_name
    else:
        raise ValueError(
            f"Could not resolve country name for code: {country_code}")


class ArtistCountryPlugin(BeetsPlugin):

    def __init__(self):
        super(ArtistCountryPlugin, self).__init__()
        self.config.add({
            'auto': True,
        })
        if self.config['auto'].get(bool) == True:
            self.early_import_stages = [self.imported]
        self.template_funcs['prettify_country'] = self.prettify

    def commands(self):

        # = Subcommand: artistcountry ======================

        artistcountry_subcommand = Subcommand(
            'artistcountry', aliases=("ac",), help=f'Fetch {COUNTRY_KEY} and save it in library')
        artistcountry_subcommand.parser.add_album_option()
        artistcountry_subcommand.parser.add_option(
            '-m', '--manual',
            action="store",
            type="string",
            help=f'manually set the {COUNTRY_KEY} to the specified iso3166-1 alpha_2 country code from pycountry. Use together with the -a argument.',
        )
        artistcountry_subcommand.parser.add_option(
            '-p', '--pretend',
            action="store_true",
            help='dry run without applying changes',
        )

        def _artistcountry_subcommand_function(lib, opts, args):
            arguments = ui.decargs(args)
            selection = None
            if arguments:
                if opts.album:
                    selection = lib.albums(arguments)
                else:
                    selection = lib.items(arguments)
            else:
                selection = lib.albums()

            for model in selection:
                target_country = None
                if opts.manual:
                    target_country = model[COUNTRY_KEY] = opts.manual
                else:
                    target_country = self._fetch_country_from_mb(model)
                if not target_country:
                    continue

                model[COUNTRY_KEY] = target_country
                changed = self._show_diff(model)
                if changed and not opts.pretend and ui.input_yn('Update? (Y/n)?', require=False):
                    if self._log.isEnabledFor(logging.DEBUG):
                        self._log.debug(
                            f'Updating {COUNTRY_KEY} for: {format(model)}')
                    model.store()
                    if isinstance(model, Album):
                        if self._log.isEnabledFor(logging.DEBUG):
                            self._log.debug(
                                f'Updating {COUNTRY_KEY} for all tracks in album: {format(model)}')
                        self._align_album_item_countries(model)

        artistcountry_subcommand.func = _artistcountry_subcommand_function
        return [artistcountry_subcommand]

    def imported(self, session, task):
        model = task.album if task.is_album else task.item
        try:
            target_country = self._fetch_country_from_mb(model)
            if not target_country:
                return
            self._store_if_necessary(target_country, model)
            if task.is_album:
                self._align_album_item_countries(model)
        except ValueError as e:
            self._log.warn(str(e))

    def _store_if_necessary(self, country_code, model):
        is_different = _is_country_different(country_code, model)
        if is_different:
            model[COUNTRY_KEY] = country_code
            model.store()
        else:
            if self._log.isEnabledFor(logging.DEBUG):
                self._log.debug(
                    f"{COUNTRY_KEY} {country_code} was already up to date for {model.albumartist}.")
        return is_different

    def _fetch_country_from_mb(self, model) -> str:
        country_code = None
        try:
            country_code = _fetch_country_data(model)
            if country_code:
                if self._log.isEnabledFor(logging.DEBUG):
                    self._log.debug(
                        f"{COUNTRY_KEY} {country_code} resolved for {model.albumartist}.")
        except ValueError as e:
            self._log.warn(ui.colorize("text_warning", str(e)))
        return country_code

    def _align_album_item_countries(self, album):
        album_value = album[COUNTRY_KEY]
        for item in album.items():
            self._store_if_necessary(album_value, item)

    def _show_diff(self, model):
        model_changed = ui.show_model_changes(model)
        if isinstance(model, Album):
            for item in model.items():
                temp_item = copy(item)
                temp_item[COUNTRY_KEY] = model[COUNTRY_KEY]
                model_changed = ui.show_model_changes(temp_item) \
                    or model_changed
        return model_changed

    def prettify(self, text: str) -> str:
        try:
            return prettify_country_name(text)
        except ValueError as e:
            self._log.warn(ui.colorize("text_warning", str(e)))


def _collect_country_values(selection):
    country_values = set()
    country_unset_detected = False
    for model in selection:
        if COUNTRY_KEY in model and model[COUNTRY_KEY] != "":
            country_values.add(model[COUNTRY_KEY])
        elif not country_unset_detected:
            country_unset_detected = True
    return country_values, country_unset_detected


def memoize_artist(f):
    cache = {}

    def memf(item):
        artist_id = ''
        if 'mb_albumartistid' in item and item['mb_albumartistid'] != '':
            artist_id = item['mb_albumartistid']
        elif 'mb_artistid' in item and item['mb_artistid'] != '':
            artist_id = item['mb_artistid']
        else:
            artist_id = UNKNOWN_ARTIST_ID
        if artist_id not in cache:
            cache[artist_id] = f(item)
        return cache[artist_id]
    return memf


@memoize_artist
def _fetch_country_data(item):
    error_prefix = "Could not resolve artist country from MusicBrainz"
    artist_key = ''
    if 'mb_albumartistid' in item and item['mb_albumartistid'] != '':
        artist_key = 'mb_albumartistid'
    elif 'mb_artistid' in item and item['mb_artistid'] != '':
        artist_key = 'mb_artistid'
    else:
        raise ValueError(
            f"{error_prefix}: No mb_albumartistid/mb_artistid in {format(item)}")
    try:
        artist_item = get_artist_by_id(item[artist_key])
    except ResponseError as e:
        raise ValueError(
            f"{error_prefix}: Invalid artist, {artist_key} '{item[artist_key]}'", e)
    artist = artist_item['artist']
    country_code = artist.get('country', '')
    if not country_code:
        try:
            country_code = _country_from_area(artist['area'])
        except Exception as e:
            raise ValueError(
                f"{error_prefix}: No country data in MusicBrainz for {artist['name']}", e)
    return country_code


def _country_from_area(area):
    countries = _find_top_area(area)
    return countries[0]


def _find_top_area(area):
    new_area = get_area_by_id(area['id'], includes=['area-rels'])
    new_area = [
        a['area'] for a in new_area['area']['area-relation-list']
        if a.get('direction', '') == 'backward'
    ]
    if not new_area:
        return area['iso-3166-1-code-list']
    return _find_top_area(new_area[0])


def _is_country_different(country_code, model):
    return COUNTRY_KEY not in model or model[COUNTRY_KEY] != country_code
